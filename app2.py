from Base import Base
from flask import Flask,request,jsonify
from sqlalchemy import Column,Integer,String,BigInteger,ForeignKey,create_engine
from sqlalchemy.orm import relationship,backref,sessionmaker
from Employee import Employee
from Department import Department

engine = create_engine("postgresql://consultadd:consultadd@localhost/flaskproject",echo=True)

app = Flask(__name__)

Base.metadata.create_all(engine)

Session = sessionmaker(engine)
session = Session()



@app.route('/')
def hello():
    return "Hello Consultadd"

@app.route('/employee',methods=['POST'])
def addEmployee():
    dic = request.form
    employee = Employee(name=dic['name'],income=dic['income'])
    session.add(employee)
    session.commit()
    print(employee.name)
    print(employee.income)
    return "Successfilly Added"


@app.route('/employee/add-department/<id>',methods=['PUT'])
def addDepartmentOfEmployee(id):
    department = request.form['department']
    fetchedDepartment = session.query(Department).where(Department.name==department).first()
    fetchedEmployee = session.query(Employee).where(Employee.id==id).first()

    if fetchedEmployee is not None:
        if fetchedDepartment is not None:
            fetchedEmployee.department = fetchedDepartment
            session.add(fetchedEmployee)
            session.commit()
            return "successfully department of "+fetchedEmployee.name+" added"

        return "Entered Department is not available"

    return "Employee not registered"


@app.route("/employee/<name>")
def getEmployee(name):
    employee = session.query(Employee).where(Employee.name==name).first();
    if employee is not None:
        fetchedemployee = {};
        fetchedemployee['id'] = employee.id
        fetchedemployee['name'] = employee.name
        fetchedemployee['income'] = employee.income
        return jsonify(fetchedemployee)

    return "Employee not available"



@app.route('/department',methods=['POST'])
def addDepartment():
    dic = request.form
    department = Department(name=dic['name'])
    session.add(department)
    session.commit()
    return "Department successfilly Added"


@app.route('/department/add-manager/<name>',methods=['PUT'])
def addManagerOfDepartment(name):
    id = request.form['id']
    fetchedDepartment = session.query(Department).where(Department.name==name).first()
    fetchedEmployee = session.query(Employee).where(Employee.id==id).first()

    if fetchedEmployee is not None:
        if fetchedDepartment is not None:
            fetchedDepartment.manager = fetchedEmployee
            session.add(fetchedDepartment)
            session.commit()
            return "successfully manager of "+fetchedDepartment.name+" added"

        return "Entered Department is not available"

    return "Employee not registered"

@app.route('/department/<name>/employees')
def getAllEmployeesOfDepartment(name):
    department = session.query(Department).where(Department.name==name).first()
    employees = []
    if department is not None:
        for employee in department.employees:
            fetchedemployee = {};
            fetchedemployee['id'] = employee.id
            fetchedemployee['name'] = employee.name
            fetchedemployee['income'] = employee.income
            employees.append(fetchedemployee)
        return jsonify(employees)


    return "Department not available"


@app.route('/employee/<id>/manager')
def getManagerOfEmployee(id):
    fetchedEmployee = session.query(Employee).where(Employee.id == id).first()
    print(fetchedEmployee.department.manager.name)
    employee = fetchedEmployee.department.manager
    fetchedemployee = {};
    fetchedemployee['id'] = employee.id
    fetchedemployee['name'] = employee.name
    fetchedemployee['income'] = employee.income
    return jsonify(fetchedemployee)

if __name__ == '__main__':
    app.run(debug=True)