from flask import Flask,request,jsonify
from sqlalchemy import create_engine,MetaData,Table,Column,Integer,String,BigInteger

engine = create_engine("postgresql://consultadd:consultadd@localhost/flaskproject",echo=True)
meta = MetaData();

user = Table('user',meta,
             Column('id',Integer,primary_key=True),
             Column('name',String,unique=True),
             Column('income',BigInteger)
             )

meta.create_all(engine)

conn = engine.connect();





app = Flask(__name__)



@app.route('/user/<name>')
def getUser(name):
    select = user.select().where(user.c.name==name)
    result = conn.execute(select)
    for row in result:
        fetcheduser = {};
        fetcheduser['id'] = row.id
        fetcheduser['name'] = row.name
        fetcheduser['income'] = row.income
        return jsonify(fetcheduser)
    return "Not Found"


@app.route('/user')
def getUsers():
    select = user.select();
    result = conn.execute(select)
    users = []
    for row in result:
        fetcheduser = {};
        fetcheduser['id'] = row.id
        fetcheduser['name'] = row.name
        fetcheduser['income'] = row.income
        users.append(fetcheduser)
    return jsonify(users)


@app.route('/user',methods=["POST"])
def addUser():
    inputUser = request.form
    ins = user.insert().values(name=inputUser['name'], income=inputUser['income'])
    result = conn.execute(ins)
    return "successfully added"

if __name__ == '__main__':
    app.run(debug=True)

app.debug=True