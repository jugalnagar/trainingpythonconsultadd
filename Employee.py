from Base import Base
from sqlalchemy import Column,Integer,String,BigInteger,ForeignKey
from sqlalchemy.orm import relationship,backref



class Employee(Base):
    __tablename__="employee"

    id = Column(Integer,primary_key=True)
    name = Column(String(50))
    income = Column(BigInteger)
    department_id = Column(Integer,ForeignKey('department.id'))
    department = relationship("Department", backref="employees", lazy='select',foreign_keys=[department_id])








