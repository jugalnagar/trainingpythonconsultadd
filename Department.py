from Base import Base
from sqlalchemy import Column,Integer,String,ForeignKey
from sqlalchemy.orm import relationship,backref


class Department(Base):
    __tablename__="department"

    id = Column(Integer,primary_key=True)
    name = Column(String(50))
    manager_id = Column(Integer,ForeignKey('employee.id'))
    manager = relationship("Employee", backref="department_manager", lazy='select', uselist=False,foreign_keys=[manager_id])
